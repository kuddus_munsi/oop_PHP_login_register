<?php
include 'resource/header.php'
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2>User Login</h2>
    </div>
    <div class="panel-body">
        <form action="" method="post">
            <div class="form-group">
                <label for="email">Email Address:</label>
                <input type="text" name="email" id="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="text" name="password" id="password" class="form-control">
            </div>
            <button class="btn btn-success" type="submit" name="login">Login</button>
        </form>
    </div>
</div>



<?php
include 'resource/footer.php'
?>
