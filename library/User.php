<?php
 include_once  'Session.php';
 include  'Database.php';
class User
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    public  function userRegistration($data)
    {

        $name = $data['name'];
        $user = $data['user'];
        $email = $data['email'];
        $password = md5($data['password']);
        $email_check = $this->emailCheck($email);

        if(empty($name) OR empty($user) OR empty($email) OR empty($password)){

            $msg  = "<div class='btn btn-danger'><strong>Error! </strong> input field should not empty</div>";
            echo $msg;
        }

        if(strlen($user) <3 ){
            $msg  = "<div class='btn btn-danger'><strong>Error! </strong> user name too short</div>";
            echo $msg;
        }elseif (preg_match('/[^a-z0-9_-]/i' ,$user)){

            $msg  = "<div class='btn btn-danger'><strong>Error! </strong> user name accept only alphanumeric, numeric, underscore and dhases only.</div>";
            echo $msg;
        }

        if (filter_var($email , FILTER_VALIDATE_EMAIL) === false){
            $msg  = "<div class='btn btn-danger'><strong>Error! </strong> email address is not valid.</div>";
            echo $msg;
        }

        if($email_check == true){
            $msg  = "<div class='btn btn-danger'><strong>Error! </strong> email address already exist.</div>";
            echo $msg;
        }
    }

    public  function emailCheck($email){

        $sql = "SELECT email FROM user_data WHERE email = :email";
        $query = $this->db->pdo->prepare($sql);
        $query->bindValue(':email' , $email);
        $query->execute();
        if($query->rowCount() >0){

            return true;
        }
        else {
            return false;
        }
    }

}


