<?php


class Database
{
    public $pdo;
    private $hostName = "localhost";
    private $dbUser = "root";
    private $dbName = "db_lr";
    private $password = "";

    public function __construct()
    {
        if(!isset($this->pdo)){
        try{
            $connection = new PDO("mysql:host=" . $this->hostName."; dbname =".$this->dbName,$this->dbUser,$this->password);
            $connection ->setAttribute(PDO::ATTR_ERRMODE , PDO::ERRMODE_EXCEPTION);
            $connection->exec("SET CHARACTER SET utf8");
            $this->pdo = $connection;
        }catch (PDOException $e){
            die("faild to connect database" . $e->getMessage());
        }
    }
    }
}
