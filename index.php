<?php
include 'resource/header.php';
include 'library/User.php';
$user = new User();
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2>User list <span class="pull-right"><strong>Welcome</strong>php</span></h2>
    </div>
    <div class="panel-body">
        <table class=" table table-striped">
            <tr>
                <th width="20%">serial </th>
                <th width="20%">Name </th>
                <th width="20%">User name </th>
                <th width="20%">Email Address </th>
                <th width="20%">Action </th>
            </tr>
            <tr>
                <td>01</td>
                <td>PHP</td>
                <td>zend</td>
                <td>php@php.com</td>
                <td><a class="btn btn-primary" href="profile.php?id =1">view</a></td>
            </tr>

            <tr>
                <td>01</td>
                <td>PHP</td>
                <td>zend</td>
                <td>php@php.com</td>
                <td><a class="btn btn-primary" href="profile.php?id =2">view</a></td>
            </tr>

            <tr>
                <td>01</td>
                <td>PHP</td>
                <td>zend</td>
                <td>php@php.com</td>
                <td><a class="btn btn-primary" href="profile.php?id =3">view</a></td>
            </tr>

        </table>
    </div>
</div>



<?php
include 'resource/footer.php'
?>
