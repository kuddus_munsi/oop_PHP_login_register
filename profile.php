<?php
include 'resource/header.php'
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2>User profile <span class="pull-right"><a  class="btn btn-primary" href="index.php">Back</a></span></h2>
    </div>
    <div class="panel-body">
        <form action="" method="post">

            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" name="name" id="name" class="form-control">
            </div>

            <div class="form-group">
                <label for="user">Email Address:</label>
                <input type="text" name="user" id="user" class="form-control">
            </div>


            <div class="form-group">
                <label for="email">Email Address:</label>
                <input type="text" name="email" id="email" class="form-control">
            </div>

            <button class="btn btn-success" type="submit" name="update">update</button>
        </form>
    </div>
</div>



<?php
include 'resource/footer.php'
?>
