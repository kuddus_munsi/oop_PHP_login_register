<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Login Register System </title>
    <link rel="stylesheet" href="resource/bootstrap.min.css">
    <script src="resource/bootstrap.min.js"></script>
    <script src="resource/jquery-3.2.1.min.js"></script>
</head>
<body>
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a href="index.php" class="navbar-brand"> PHP Login Register</a>
                    </div>
                    <ul class="nav navbar-nav pull-right">
                        <li><a href="profile.php">Profile</a></li>
                        <li><a href="logout.php">Logout</a></li>
                        <li><a href="login.php">Login</a></li>
                        <li><a href="register.php">Register</a></li>
                    </ul>
                </div>
            </nav>